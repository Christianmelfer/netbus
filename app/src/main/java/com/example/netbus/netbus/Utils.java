package com.example.netbus.netbus;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ExpoCode Tech http://expocodetech.com
 */
public class Utils {
    public static String getCurrentDateTime(){
        String currentDate = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.US);
        // Get the date today using Calendar object.
        Date today = Calendar.getInstance().getTime();
        // Using DateFormat format method we can create a string
        // representation of a date with the defined format.
        currentDate = df.format(today);
        return currentDate;
    }

    public interface OnSelectListener {
        void onSelectItem(DialogInterface dialog, int which);
    }

    public static void selectOption(Activity activity, int resTitle, final int resArrOptions, final OnSelectListener listener){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(resTitle)
                .setItems(resArrOptions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onSelectItem(dialog,which);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}