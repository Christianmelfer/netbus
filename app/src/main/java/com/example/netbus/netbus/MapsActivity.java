package com.example.netbus.netbus;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.netbus.netbus.models.LastLocation;
import com.example.netbus.netbus.models.Usuarios;
import com.firebase.client.Firebase;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Iterator;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback,
        GoogleMap.OnMapLongClickListener {

    private static final String TAG = MapsActivity.class.getName();
    private static int MY_LOCATION_REQUEST_CODE = 1;
    private static int CAMERA_ZOOM = 16;
    private static int INTERVAL = 10000;
    private static int FAST_INTERVAL = 5000;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private LatLng mLocForSpot;

    private DatabaseReference mDatabase;
    private ArrayList<Usuarios> mUsuarios;
    private Firebase mRootUserRef;
    private static final String USER_REF = "https://Netbus-9d4d0.firebaseio.com/Usuarios";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if(mapFragment!=null){
            mapFragment.getMapAsync(this);
        }

        Firebase.setAndroidContext(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        initGoogleAPIClient();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        mRootUserRef = new Firebase(USER_REF);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        mRequestingLocationUpdates = false;
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }


    private void initGoogleAPIClient(){
        //Este método permite iniciarlizar el Objeto Cliente que consumira el servicio
        //de API de Google
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            //Creamos una peticion de ubicacion con el objeto LocationRequest
            createLocationRequest();
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FAST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.setOnMapLongClickListener(this);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }

            getUsuariosFromFirebase();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (!mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onResult(@NonNull Result result) {

    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mRequestingLocationUpdates = true;
        }
    }

    private void stopLocationUpdates() {
        if (mRequestingLocationUpdates) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            mRequestingLocationUpdates = false;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mMap != null){
            LatLng newLatLng=new LatLng (location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newLatLng,15));

        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    private void getUsuariosFromFirebase() {
        //Obtenemos la referncia a la entidad Usuarios dentro de la base de Firebase
        //DatabaseReference refUsuarios = FirebaseDatabase.getInstance().getReference("Usuarios");
        mUsuarios = new ArrayList<Usuarios>();
        mDatabase.child("Usuarios").addValueEventListener(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange");
                mUsuarios.clear();
                try {
                    //Cuando este método se ejecuta es por que hemos realizado una peticion
                    //exitosa al al base de datos firebase por lo tanto como lo que estamos buscando
                    // es todos lo usuarios debemos iterar sobre los mismos mediante la siguiente sentencia
                    Iterator<com.google.firebase.database.DataSnapshot> it = dataSnapshot.getChildren().iterator();
                    while (it.hasNext()) {
                        // En cada iteración obtenemos el siguiente registro
                        com.google.firebase.database.DataSnapshot ds = it.next();
                        // De cada registro extraemos los datos de Usuarios los cuales mapean con
                        // nuestro modelo Usuarios.java
                        Usuarios aUsuarios = ds.getValue(Usuarios.class);
                        if (aUsuarios != null) {
                            //Se teamos el ID del registro como valor de la propiedad Id del
                            // modelo Usuario
                            aUsuarios.setId(ds.getKey());
                            //Agregamos el registro al ArrayList de Usuarios
                            mUsuarios.add(aUsuarios);
                        }
                    }
                    if (mUsuarios.size() > 0){
                        //Si hemos obtenido al menos un usaurio llamamos la método que
                        // dibuja los markers en el Gogole Map
                        drawMarkersOnMap();
                    }
                }catch (Exception e){
                    Log.e(TAG, e.getMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "onCancelled " + databaseError.getMessage());
            }
        });
    }

    private void drawMarkersOnMap(){
        if (mMap != null) {
            //Limpiamos el mapa, con este método limpiamos borramos todos los markers
            mMap.clear();
            //Iteramos sobre cada Usuario obtenido dela base de datos de Firabase y
            //por cada Usuario construimos un Marker que agregamos al mapa para indicar la
            //ubicación del usuario.
            for (int i=0; i < mUsuarios.size(); i++){
                Usuarios user = mUsuarios.get(i);
                LatLng pos = new LatLng(user.getLatitud(), user.getLongitud());
                mMap.addMarker(new MarkerOptions()
                        .position(pos)
                        .title(user.getNbus())  //Agrega un titulo al marcador
                        .snippet(user.getRbus())   //Agrega información detalle relacionada con el marcador
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));

            }
        }

    }
}