package com.example.netbus.netbus;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by Jessica on 18/01/2017.
 */

public class Netbus extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}
