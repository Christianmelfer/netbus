package com.example.netbus.netbus;

import java.net.PortUnreachableException;

/**
 * Created by expocodetech on 9/2/17.
 */

public final class Constants {
    public static final String SP_FILE = "com.example.netbus.netbus.defaultfile";
    public static final String SP_DRIVER_KEY = "driverKey";
    public static final String SP_DRIVER_NAME = "driverName";
    public static final String SP_DRIVER_MODEL = "driverModel";
    public static final String SP_DRIVER_IMAGE = "driverImage";

    public static final String SP_USER_KEY = "userKey";
    public static final String SP_USER_NAME = "userName";
    public static final String SP_USER_DETAILS = "userDetails";


    public static final String FB_BUCKET_IMAGES_ROOT = "gs://netbus-9d4d0.appspot.com/images";
    public static final String FB_BUCKET_ICONS_ROOT = "gs://netbus-9d4d0.appspot.com/icons";
    public static final String FB_DB_DRIVERS = "drivers";  //Conductores
    public static final String FB_DB_USERS = "users";      //Peatones

    public static final String DMA_JSON_ENDPOINT= "https://maps.googleapis.com/maps/api/distancematrix/json";      //Peatones

    public static final String[] EMAILS = {"expocodetech@gmail.com"};

}
