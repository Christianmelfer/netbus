package com.example.netbus.netbus.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by expocodetech on 14/2/17.
 */

@IgnoreExtraProperties
public class User {
    private String name;
    private String details;
    private Double latitud;
    private Double longitud;
    private boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
