package com.example.netbus.netbus.models;

/**
 * Created by Jessica on 19/01/2017.
 */

public class Usuarios {
    String id;
    double latitud;
    double longitud;
    String nbus;
    String rbus;

    public Usuarios() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getNbus() {
        return nbus;
    }

    public void setNbus(String nbus) {
        this.nbus = nbus;
    }

    public String getRbus() {
        return rbus;
    }

    public void setRbus(String rbus) {
        this.rbus = rbus;
    }
}

