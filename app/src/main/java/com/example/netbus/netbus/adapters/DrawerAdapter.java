package com.example.netbus.netbus.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.netbus.netbus.Constants;
import com.example.netbus.netbus.R;
import com.example.netbus.netbus.models.CarImage;
import com.example.netbus.netbus.models.Driver;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

/**
 * Created by expocodetech on 20/2/17.
 */

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {

    public interface DrawerAdapterListener {
        void onSelectedItem(Driver driver);
    }

    private Activity mHostActivity;
    private DrawerAdapterListener  mListener;
    private ArrayList<Driver> mDrivers;
    private FirebaseStorage mStorage;

    public DrawerAdapter(ArrayList<Driver> pDrivers, Activity pHostActivity, DrawerAdapterListener pListener){
        mDrivers = pDrivers;
        mHostActivity = pHostActivity;
        mListener = pListener;
        mStorage = FirebaseStorage.getInstance();
    }

    @Override
    public DrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drawer_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DrawerAdapter.ViewHolder holder, int position) {
        Driver aDriver = mDrivers.get(position);
        holder.driver = aDriver;

        StorageReference storageRef = mStorage.getReferenceFromUrl(Constants.FB_BUCKET_IMAGES_ROOT).child(aDriver.getImage());
        Glide.with(mHostActivity)
                .using(new FirebaseImageLoader())
                .load(storageRef)
                .into(holder.mIVDriverImage);

        holder.mTVDriverModel.setText(aDriver.getDetails());
        holder.mTVDriverName.setText(aDriver.getName());
        holder.mTVDriverDuration.setText(aDriver.getDistanceTime());
        holder.mCheckbox.setChecked(aDriver.isDisplay());

        holder.mItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.driver.isDisplay()) {
                    holder.driver.setDisplay(false);
                } else {
                    holder.driver.setDisplay(true);
                }
                holder.mCheckbox.setChecked(holder.driver.isDisplay());
                mListener.onSelectedItem(holder.driver);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDrivers.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        final View mItemView;
        final ImageView mIVDriverImage;
        final TextView mTVDriverModel;
        final TextView mTVDriverName;
        final TextView mTVDriverDuration;
        final CheckBox mCheckbox;
        Driver driver;

        ViewHolder(View view){
            super(view);
            mItemView = view;
            mIVDriverImage = (ImageView) view.findViewById(R.id.carDriverImage);
            mTVDriverModel = (TextView) view.findViewById(R.id.carDriverModel);
            mTVDriverName = (TextView) view.findViewById(R.id.carDriverName);
            mTVDriverDuration = (TextView) view.findViewById(R.id.carDriverDuration);
            mCheckbox = (CheckBox) view.findViewById(R.id.carDriverCheck);
        }
    }
}
