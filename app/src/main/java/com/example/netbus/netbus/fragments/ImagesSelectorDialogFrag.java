package com.example.netbus.netbus.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.example.netbus.netbus.Constants;
import com.example.netbus.netbus.R;
import com.example.netbus.netbus.adapters.ImageSelectorAdapter;
import com.example.netbus.netbus.models.CarImage;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by expocodetech on 6/2/17.
 */

public class ImagesSelectorDialogFrag extends DialogFragment implements ImageSelectorAdapter.ImageSelectorAdapterListener {
    private static final String TAG = ImagesSelectorDialogFrag.class.getName();
    private DatabaseReference mDatabase;

    public interface ImagesSelectorDialogListener {
        void onImageSelected(DialogFragment dialogFragment, CarImage carImage);
    }

    private RecyclerView mRecyclerView;
    private ImageSelectorAdapter mAdapter;
    private ArrayList<CarImage> mCarImages;
    private ImagesSelectorDialogListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mCarImages = new ArrayList<CarImage>();

        //Descargamos las imagenes de Firebase Storage
        loadImageUrlFromFB();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.rv_images_selector, null);
        mRecyclerView = (RecyclerView) rootView;

        builder.setView(rootView);
        return builder.create();
    }

    @Override
    public void onSelectedImage(CarImage carImage) {
        Log.d(TAG, "onSelectedImage -> " + carImage.getImageName());
        mListener.onImageSelected(this,carImage);
    }

    public void setmListener(ImagesSelectorDialogListener mListener) {
        this.mListener = mListener;
    }

    private void loadImagesArray(){
        for (int i = 0; i < 20; i++) {
            CarImage carImage = new CarImage();
            carImage.setImageName("Imagen " + String.valueOf(i));
            carImage.setImageUrl("https://www.driving.co.uk/s3/st-driving-prod/uploads/2016/01/NIP000205412100-1274.jpg");
            mCarImages.add(carImage);
        }
    }

    private void loadImageUrlFromFB(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("carImages").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "loadImageUrlFromFB onDataChange");
                FirebaseStorage storage = FirebaseStorage.getInstance();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    CarImage carImage = snap.getValue(CarImage.class);
                    if (carImage.getImageName() != null && carImage.getImageName().length() > 0) {
                        StorageReference storageRef = storage.getReferenceFromUrl(Constants.FB_BUCKET_IMAGES_ROOT).child(carImage.getImageName());
                        carImage.setsRef(storageRef);
                        carImage.buildDriverAndModelName();
                        mCarImages.add(carImage);
                    }
                }
                setupAdapter();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadImageUrlFromFB onCancelled");
            }
        });
        Log.d(TAG, "loadImageUrlFromFB");
    }

    private void setupAdapter(){
        mAdapter = new ImageSelectorAdapter(mCarImages, this, getActivity());
        mRecyclerView.setAdapter(mAdapter);
    }

}
