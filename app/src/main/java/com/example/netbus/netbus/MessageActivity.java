package com.example.netbus.netbus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.example.netbus.netbus.Constants.EMAILS;

public class MessageActivity extends AppCompatActivity {
    private static final String TAG = MessageActivity.class.getName();
    private EditText mETMessageText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        mETMessageText = (EditText) findViewById(R.id.messageText);
        Button mBTSend = (Button) findViewById(R.id.btnSend);
        mBTSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
    }

    private void sendMessage(){
        if (mETMessageText != null && mETMessageText.getText().length() >  0) {
            sendEmail(mETMessageText.getText().toString());
        } else {
            Toast.makeText(this, getString(R.string.message_text_required), Toast.LENGTH_SHORT).show();
        }
    }

    private void sendEmail(String bodyEmail) {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("text/html");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, Constants.EMAILS);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_subject));
            emailIntent.putExtra(Intent.EXTRA_TEXT, bodyEmail);
            startActivity(Intent.createChooser(emailIntent,getString(R.string.chooser_title)));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
