package com.example.netbus.netbus.models;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.storage.StorageReference;

/**
 * Created by expocodetech on 6/2/17.
 */

@IgnoreExtraProperties
public class CarImage {
    private String imageName;

    private String imageUrl;
    private String modelName;
    private String driverName;
    private StorageReference sRef;

    public CarImage() {
    }

    public CarImage(String pImageUrl, String pImageName){
        this.imageName = pImageName;
        this.imageUrl = pImageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public StorageReference getsRef() {
        return sRef;
    }
    public void setsRef(StorageReference sRef) {
        this.sRef = sRef;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public void buildDriverAndModelName(){
        //BMW-Carlos_Nuñez_Cespedes.jpg
        if (this.imageName != null && this.imageName.length() > 0){
            String arrParts[] = this.imageName.split("-");
            //Seteamos el modelo del vehiculo
            setModelName(arrParts[0]);

            String nameOfDriver = arrParts[1].replace(".jpg", "");
            nameOfDriver = nameOfDriver.replace("_"," ");
            //Seteamos el nombre del conductor
            setDriverName(nameOfDriver);
        }
    }
}
