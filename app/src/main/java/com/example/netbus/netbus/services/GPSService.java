package com.example.netbus.netbus.services;

/**
 * Created by Jessica on 26/12/2016.
 */

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;

import com.example.netbus.netbus.Constants;
import com.example.netbus.netbus.models.Driver;
import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by filipp on 6/16/2016.
 */

public class GPSService extends Service {

    private static final String TAGLOG = "firebase-db";
    TelephonyManager manager;
    Firebase ref;
    private LocationListener listener;
    private LocationManager locationManager;
    private SharedPreferences mSharedPreferences;

    private String mDriverKey;
    private String mDriverName;
    private String mDriverModel;
    private String mDriverImage;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mSharedPreferences = getSharedPreferences(Constants.SP_FILE, MODE_PRIVATE);
        mDriverName = mSharedPreferences.getString(Constants.SP_DRIVER_NAME, null);
        mDriverImage = mSharedPreferences.getString(Constants.SP_DRIVER_IMAGE, null);
        mDriverModel = mSharedPreferences.getString(Constants.SP_DRIVER_MODEL, null);

        listener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                sendLocationToFB(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        //noinspection MissingPermission
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, listener);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            //noinspection MissingPermission
            locationManager.removeUpdates(listener);
        }
    }

    private void sendLocationToFB(Location location){
        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        mDriverKey = getDriverKeyFromSharedPreference();
        if (mDriverKey == null){
            mDriverKey = db.child(Constants.FB_DB_DRIVERS).push().getKey();
            setDriverKeyFromSharedPreference(mDriverKey);
        }

        Driver aDriver = new Driver();
        aDriver.setName(mDriverName);
        aDriver.setDetails(mDriverModel);
        aDriver.setImage(mDriverImage);
        aDriver.setLatitud(location.getLatitude());
        aDriver.setLongitud(location.getLongitude());
        db.child(Constants.FB_DB_DRIVERS).child(mDriverKey).setValue(aDriver);

    }

    private String getDriverKeyFromSharedPreference(){
        return mSharedPreferences.getString(Constants.SP_DRIVER_KEY,null);
    }

    private void setDriverKeyFromSharedPreference(String driverKey){
        mSharedPreferences.edit().putString(Constants.SP_DRIVER_KEY, driverKey).apply();
    }


}