package com.example.netbus.netbus;

import android.Manifest;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.netbus.netbus.adapters.DrawerAdapter;
import com.example.netbus.netbus.fragments.ImagesSelectorDialogFrag;
import com.example.netbus.netbus.models.CarImage;
import com.example.netbus.netbus.models.Driver;
import com.example.netbus.netbus.network.NetworkCtrl;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class MainActivity extends AppCompatActivity
        implements ImagesSelectorDialogFrag.ImagesSelectorDialogListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback {
    private static final String TAG = MainActivity.class.getName();
    private Button btnStartTracking, btnStopTracking;
    private Toolbar mToolbar;
    //private ImageView mIVDriverImage;
    private FirebaseStorage mStorage;

    private static int INTERVAL = 10000;
    private static int FAST_INTERVAL = 5000;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;

    private LatLng mCurrentLocation;

    private ArrayList<Driver> mDrivers;
    private DatabaseReference mDatabase;

    private String mDriverKey;
    private String mUserKey;
    private SharedPreferences mSharedPref;

    private NetworkCtrl mNetworkCtrl;
    private RecyclerView mRVDrawer;
    private DrawerAdapter mDrawerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mSharedPref = getSharedPreferences(Constants.SP_FILE, MODE_PRIVATE);
        mDriverKey = getKeyFromSharedPreference(Constants.SP_DRIVER_KEY);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mStorage = FirebaseStorage.getInstance();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(mToolbar);

        mRVDrawer = (RecyclerView) findViewById(R.id.left_drawer);
        setupLeftDrawer();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        btnStartTracking = (Button) findViewById(R.id.btnStartTrack);
        btnStopTracking = (Button) findViewById(R.id.btnStopTrack);

        if (!runtimePermissions()) {
            enableButtons();
            initGoogleAPIClient();
        }

        //Instanciamos nuestro controlador de peticiones HTTP
        mNetworkCtrl = NetworkCtrl.getInstance(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        mRequestingLocationUpdates = false;
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionImage:
                loadImageSelector();
                return true;
            case R.id.actionMessage:
                goToMessageActivity();
                return true;
            case R.id.actionSettings:
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void enableButtons() {
        btnStartTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTrackingLocation();
            }
        });

        btnStopTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopTrackingLocation();
            }
        });
    }

    private void startTrackingLocation() {
        //Inicio de la detección y envio de la ubicacion
        /*Intent i = new Intent(getApplicationContext(), GPSService.class);
        startService(i);*/
        mDriverKey = mSharedPref.getString(Constants.SP_DRIVER_KEY,null);
        if (mDriverKey == null){
            mDriverKey = mDatabase.child(Constants.FB_DB_DRIVERS).push().getKey();
            mSharedPref.edit().putString(Constants.SP_DRIVER_KEY,mDriverKey).apply();
        }
    }


    private void sendLocationToFB(Location location){
        String lDriverName = mSharedPref.getString(Constants.SP_DRIVER_NAME, null);
        String lDriverImage = mSharedPref.getString(Constants.SP_DRIVER_IMAGE, null);
        String lDriverModel = mSharedPref.getString(Constants.SP_DRIVER_MODEL, null);

        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        Driver aDriver = new Driver();
        aDriver.setName(lDriverName);
        aDriver.setDetails(lDriverModel);
        aDriver.setImage(lDriverImage);
        aDriver.setLatitud(location.getLatitude());
        aDriver.setLongitud(location.getLongitude());
        db.child(Constants.FB_DB_DRIVERS).child(mDriverKey).setValue(aDriver);
    }


    private void stopTrackingLocation() {
        //Detener la detección y envio de la ubicacion
        /*Intent i = new Intent(getApplicationContext(), GPSService.class);
        stopService(i);*/

        mDriverKey = null;
    }

    private boolean runtimePermissions() {
        if (Build.VERSION.SDK_INT >= 23
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
            return true;
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                enableButtons();
            } else {
                runtimePermissions();
            }
        }
    }

    private void loadImageSelector() {
        ImagesSelectorDialogFrag imgSelectorDialog = new ImagesSelectorDialogFrag();
        imgSelectorDialog.setmListener(this);
        imgSelectorDialog.show(getFragmentManager(), "ImageSelectorDialog");
    }


    @Override
    public void onImageSelected(DialogFragment dialogFragment, CarImage carImage) {
        Log.d(TAG, "onImageSelected -> " + carImage.getImageName());
        saveDriverImageInSharedPreference(carImage);
        startTrackingLocation();
        dialogFragment.dismiss();
    }

    private void saveDriverImageInSharedPreference(CarImage carImage) {
        //Obtenemos uan referencia al fichero de Preferencias compartidas
        // y si todavia no existe dicho fichero se crea.
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SP_FILE, MODE_PRIVATE);
        //Una vez obtenida la referencia al fichero de  Preferencias compartidas procedemos a
        // editarlo agregando el nombre de la imagen bajo la clave SP_DRIVER_IMAGE_NAME, el modelo
        // y el nombre del conductor
        sharedPreferences.edit().putString(Constants.SP_DRIVER_MODEL, carImage.getModelName()).apply();
        sharedPreferences.edit().putString(Constants.SP_DRIVER_NAME, carImage.getDriverName()).apply();
        sharedPreferences.edit().putString(Constants.SP_DRIVER_IMAGE, carImage.getImageName()).apply();
        //loadImageDriver(carImage.getImageName());
    }

    private void initGoogleAPIClient() {
        //Este método permite iniciarlizar el Objeto Cliente que consumira el servicio
        //de API de Google
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            //Creamos una peticion de ubicacion con el objeto LocationRequest
            createLocationRequest();
            //getLastLocation();
        }
    }

    private void getLastLocation() {
        if (mCurrentLocation == null) {
            if (Build.VERSION.SDK_INT >= 23
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
            } else {
                Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                mCurrentLocation = new LatLng(loc.getLatitude(), loc.getLongitude());
            }
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FAST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            //mMap.setOnMapLongClickListener(this);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                getDriversFromFirebase();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (!mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onResult(@NonNull Result result) {

    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mRequestingLocationUpdates = true;
        }
    }

    private void stopLocationUpdates() {
        if (mRequestingLocationUpdates) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            mRequestingLocationUpdates = false;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mMap != null){
            mCurrentLocation = new LatLng (location.getLatitude(), location.getLongitude());
            if (mDriverKey == null) {
                calculateDistance();
            } else {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation,15));
            }
            //sendLocationToUserFB(location);
        }
    }


/*    private void sendLocationToUserFB(Location location){
        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        mDriverKey = getKeyFromSharedPreference(Constants.SP_DRIVER_KEY);
        if (mDriverKey != null)
            return;

        mUserKey =  getKeyFromSharedPreference(Constants.SP_USER_KEY);
        if (mUserKey == null){
            mUserKey = db.child(Constants.FB_DB_USERS).push().getKey();
            setKeyToSharedPreference(Constants.SP_USER_KEY, mUserKey);
        }

        if (mUserKey != null) {
            User aUser= new User();
            aUser.setName("Javier");
            aUser.setDetails("Peaton");
            aUser.setLatitud(location.getLatitude());
            aUser.setLongitud(location.getLongitude());
            aUser.setActive(true);
            db.child(Constants.FB_DB_USERS).child(mUserKey).setValue(aUser);
        }
    }*/

    private String getKeyFromSharedPreference(String key){
        return mSharedPref.getString(key,null);
    }


    private void setKeyToSharedPreference(String key, String valueKey){
        mSharedPref.edit().putString(key, valueKey).apply();
    }

    private void getDriversFromFirebase() {
        //Obtenemos una referncia al nodo "drivers" y agregamos un ValueEventListener
        mDatabase.child(Constants.FB_DB_DRIVERS).addValueEventListener(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange");
                boolean displayAll = false;
                if (mDrivers != null) {
                    if (mDrivers.size() == 0) {
                        displayAll = true;
                    }
                    mDrivers.clear();
                }

                try {
                    String driverKey = mSharedPref.getString(Constants.SP_DRIVER_KEY, null);
                    //Cuando este método se ejecuta es por que hemos realizado una peticion
                    //exitosa al al base de datos firebase por lo tanto como lo que estamos buscando
                    // es todos lo usuarios debemos iterar sobre los mismos mediante la siguiente sentencia
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        // En cada iteración obtenemos el siguiente registro
                        // De cada registro extraemos los datos de Usuarios los cuales mapean con
                        // nuestro modelo Usuarios.java
                        Driver aDriver = ds.getValue(Driver.class);
                        if (aDriver != null) {
                            //Se teamos el ID del registro como valor de la propiedad Id del
                            // modelo Usuario
                            aDriver.setId(ds.getKey());

                            if (displayAll) {
                                aDriver.setDisplay(displayAll);
                            } else {
                                //Evaluar cuales son los drivers que el usuario a ocultado
                                //TODO: establecer la logica necesaria para decidir si el atributo display de un Driver es true o false

                            }

                            if (driverKey != null && !driverKey.equalsIgnoreCase(aDriver.getId()))
                                mDrivers.add(aDriver);
                            else if (driverKey == null)
                                mDrivers.add(aDriver);

                            //Agregamos el registro al ArrayList de Usuarios
                            //mDrivers.add(aDriver);
                        }
                    }
                    if (mDrivers.size() > 0){
                        calculateDistance();
                        //Si hemos obtenido al menos un usaurio llamamos la método que
                        // dibuja los markers en el Gogole Map
                        drawMarkersOnMap();
                    }
                }catch (Exception e){
                    Log.e(TAG, e.getMessage());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "onCancelled " + databaseError.getMessage());
            }
        });
    }

    private void drawMarkersOnMap(){
        if (mMap != null) {
            //Limpiamos el mapa, con este método limpiamos borramos todos los markers
            mMap.clear();
            //Iteramos sobre cada Driver obtenido de la base de datos de Firabase y
            //por cada Driver construimos un Marker que agregamos al mapa para indicar la
            //ubicación del mismo.
            for (int i=0; i < mDrivers.size(); i++){
                Driver aDriver = mDrivers.get(i);
                LatLng pos = new LatLng(aDriver.getLatitud(), aDriver.getLongitud());
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(pos)
                        .title(aDriver.getName())  //Agrega un titulo al marcador
                        .snippet(aDriver.getDetails()));   //Agrega información detalle relacionada con el marcador
                aDriver.setMarker(marker);
                loadMarkerIcon(marker, aDriver.getImage());
            }
        }
    }

    private void loadMarkerIcon(final Marker marker, String imageName) {
        //marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car_icon_20x20));
        if (imageName == null)
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        else {
            StorageReference storageRef = mStorage.getReferenceFromUrl(Constants.FB_BUCKET_ICONS_ROOT).child(imageName);
            Glide.with(this)
                    .using(new FirebaseImageLoader())
                    .load(storageRef)
                    .asBitmap().fitCenter().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);
                    marker.setIcon(icon);
                }
            });
        }
    }

    private void calculateDistance() {
        //mCurrentLocation = Ubicacion del usuario peaton
        //mDrivers = Listado de Drivers qeu se obtuvieron de Firebase con sus respectivas ubicaciones
        if (mDrivers != null && mDrivers.size() >0 && mCurrentLocation != null) {
            Double userLatitud = mCurrentLocation.latitude;
            Double userLongitud = mCurrentLocation.longitude;
            String destino = String.valueOf(userLatitud).concat(",").concat(String.valueOf(userLongitud));

            for (int i=0; i < mDrivers.size(); i++) {
                Driver aDriver = mDrivers.get(i);
                reqDistanceCalculation(aDriver,  destino);
            }
        }
    }

    private void reqDistanceCalculation(final Driver driver, String destino){
        Double driverLat = driver.getLatitud();
        Double driverLong = driver.getLongitud();
        String origen = String.valueOf(driverLat).concat(",").concat(String.valueOf(driverLong));

        //https://maps.googleapis.com/maps/api/distancematrix/json?
        // origins=
        // &destinations=
        // &mode=driving
        // &language=es-ES
        // &key=YOUR_API_KEY
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("maps.googleapis.com")
                .appendPath("maps")
                .appendPath("api")
                .appendPath("distancematrix")
                .appendPath("json")
                .appendQueryParameter("origins", origen)
                .appendQueryParameter("destinations", destino)
                .appendQueryParameter("mode", "driving")
                .appendQueryParameter("language", "es-ES")
                .appendQueryParameter("key", getResources().getString(R.string.GOOGLE_DISTANCE_API_KEY));
        String url = builder.build().toString();

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());

                        try {
                            if (response.has("status")) {
                                String status = response.getString("status");
                                if (status != null && status.equalsIgnoreCase("OK")){
                                    if (response.has("rows")) {
                                        JSONArray rows = response.getJSONArray("rows");
                                        if (rows != null && rows.length() > 0) {
                                            JSONObject row = rows.getJSONObject(0);
                                            if (row.has("elements")) {
                                                JSONArray elements = row.getJSONArray("elements");
                                                if (elements != null && elements.length() > 0) {
                                                    JSONObject element = elements.getJSONObject(0);
                                                    if (element.has("distance")) {
                                                        JSONObject distance = element.getJSONObject("distance");
                                                        if (distance.has("text"))  {
                                                            String distanceText = distance.getString("text");
                                                            driver.setDistance(distanceText);
                                                        }
                                                    }

                                                    if (element.has("duration")) {
                                                        JSONObject duration = element.getJSONObject("duration");
                                                        if (duration.has("text"))  {
                                                            String durationText = duration.getString("text");
                                                            driver.setDistanceTime(durationText);
                                                            if (driver.getMarker() != null)
                                                                driver.getMarker().setSnippet(durationText);
                                                        }

                                                        if (duration.has("value"))  {
                                                            int durationValue= duration.getInt("value");
                                                            driver.setDistanceTimeValue(durationValue);
                                                            sortDrivers();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }else {
                                    driver.setDistance(getString(R.string.distance_not_found));
                                    driver.setDistanceTime(getString(R.string.duration_not_found));
                                }

                            }
                        } catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                        } finally {
                            if (mDrawerAdapter != null)
                                mDrawerAdapter.notifyDataSetChanged();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.getMessage());
                    }
                });
        mNetworkCtrl.addToRequestQueue(jsObjRequest);
    }

    private void setupLeftDrawer(){
        if (mRVDrawer != null) {
            mDrivers = new ArrayList<Driver>();
            mDrawerAdapter = new DrawerAdapter(mDrivers, this, new DrawerAdapter.DrawerAdapterListener() {
                @Override
                public void onSelectedItem(Driver driver) {
                    Log.d(TAG, "onSelectedItem del Driver: " + driver.getName());
                    driver.getMarker().setVisible(driver.isDisplay());
                    //TODO: establecer la lógica necesaria para decidir guardar los Id de los Drivers que ocultamos
                }
            });
            mRVDrawer.setAdapter(mDrawerAdapter);
        }
    }

    private void sortDrivers() {
        Collections.sort(mDrivers, new Comparator<Driver>() {
            @Override
            public int compare(Driver driver1, Driver driver2) {
                if (driver1.getDistanceTimeValue() < driver2.getDistanceTimeValue())
                    return -1;
                else if (driver1.getDistanceTimeValue() > driver2.getDistanceTimeValue())
                    return 1;
                else
                    return 0;
            }
        });

        if (mDrawerAdapter != null)
            mDrawerAdapter.notifyDataSetChanged();

    }

    private void goToMessageActivity() {
        startActivity(new Intent(this,MessageActivity.class));
    }

}
