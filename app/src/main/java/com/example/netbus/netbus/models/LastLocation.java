package com.example.netbus.netbus.models;

/**
 * Created by Jessica on 18/01/2017.
 */

public class LastLocation {
    private Double latitude;
    private Double longitude;
    private String recordDate;

    public LastLocation(){

    }

    public LastLocation( Double lat, Double lon, String recDate){
        this.latitude = lat;
        this.longitude = lon;
        this.recordDate = recDate;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }
}
