package com.example.netbus.netbus.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by expocodetech on 16/2/17.
 */

public class NetworkCtrl {
    //mInstance es una instancia unica en toda la aplicación
    private static NetworkCtrl mInstance = null;

    private RequestQueue mRequestQueue;
    private static Context mCtx;

    public static NetworkCtrl getInstance(Context context){
        if(mInstance == null){
            mInstance = new NetworkCtrl(context);
        }
        return mInstance;
    }

    private NetworkCtrl(Context context){
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}
