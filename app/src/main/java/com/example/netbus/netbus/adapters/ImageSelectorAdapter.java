package com.example.netbus.netbus.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.netbus.netbus.R;
import com.example.netbus.netbus.models.CarImage;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

/**
 * Created by expocodetech on 6/2/17.
 */

public class ImageSelectorAdapter extends RecyclerView.Adapter<ImageSelectorAdapter.ViewHolder> {

    public interface ImageSelectorAdapterListener {
        void onSelectedImage(CarImage carImage);
    }

    private Activity mHostActivity;
    private ArrayList<CarImage> mImagesCars;
    private ImageSelectorAdapterListener mListener;

    public ImageSelectorAdapter(ArrayList<CarImage> pImagesCars, ImageSelectorAdapterListener pListener, Activity pHostActivity) {
        mImagesCars = pImagesCars;
        mListener = pListener;
        mHostActivity = pHostActivity;
    }

    @Override
    public ImageSelectorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_images_selector_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageSelectorAdapter.ViewHolder holder, int position) {
        holder.carImage = mImagesCars.get(position);

        StorageReference storageReference = mImagesCars.get(position).getsRef();
        if ( storageReference != null) {
            Glide.with(mHostActivity)
                    .using(new FirebaseImageLoader())
                    .load(storageReference)
                    .into(holder.mIVcarImage);
        }


        holder.mTVcarModel.setText(mImagesCars.get(position).getModelName());
        holder.mTVcarDriverName.setText(mImagesCars.get(position).getDriverName());

        holder.mItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onSelectedImage(holder.carImage);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImagesCars.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        final View mItemView;
        final ImageView mIVcarImage;
        final TextView mTVcarModel;
        final TextView mTVcarDriverName;
        CarImage carImage;

        ViewHolder(View view){
            super(view);
            mItemView = view;
            mIVcarImage = (ImageView) view.findViewById(R.id.carImage);
            mTVcarModel = (TextView) view.findViewById(R.id.carModel);
            mTVcarDriverName = (TextView) view.findViewById(R.id.carDriverName);
        }
    }
}
